﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ConversorMoedas
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // https://api.fixer.io/latest?base=usd

            using (var wc = new System.Net.WebClient())
            {
                string json = wc.DownloadString("https://api.fixer.io/latest?base=usd");

                var serializer = new JavaScriptSerializer();
                var moedas = (Dictionary<string, object>)(((Dictionary<string, object>)serializer.DeserializeObject(json))["rates"]);

                Moeda1.DataSource = Moeda2.DataSource = moedas;
                Moeda1.DataTextField = Moeda2.DataTextField = "Key";
                Moeda1.DataValueField = Moeda2.DataValueField = "Value";

                Moeda1.DataBind();
                Moeda2.DataBind();
            }
        }
    }
}