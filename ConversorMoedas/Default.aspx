﻿<%@ Page Title="Conversor de Moedas" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ConversorMoedas._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-12">
            <h2></h2>
            <p>
                Escolha a moeda e clique em converter:
                <asp:Label ID="Lbl" Text="" runat="server" />
            </p>
            <div class="row">
                <div class="form-inline form-group col-md-12">
                    <div class="form-group">
                        <label for="Valor1">
                            Valor: 
                        </label>
                        <asp:TextBox ID="Valor1" runat="server" CssClass="form-control money"/>
                    </div>
                    <div class="form-group">
                        <asp:DropDownList ID="Moeda1" runat="server" CssClass="form-control select">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <i class="glyphicon glyphicon-transfer"></i>
                    </div>
                    <div class="form-group">
                        <label for="Moeda2">
                            &nbsp;
                        </label>
                        <asp:DropDownList ID="Moeda2" runat="server" CssClass="form-control select">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="Valor2">
                            &nbsp;
                        </label>
                        <asp:TextBox ID="Valor2" runat="server" CssClass="form-control money"/>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="form-group">
                        <asp:Button ID="Converter" Text="Converter" runat="server" CssClass="btn btn-lg btn-primary btn-block"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
